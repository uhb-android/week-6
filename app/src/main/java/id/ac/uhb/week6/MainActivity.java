package id.ac.uhb.week6;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

import id.ac.uhb.week6.databinding.ActivityMainBinding;
import id.ac.uhb.week6.databinding.CustomDialogBinding;
import id.ac.uhb.week6.fragment.SampleBottomDialog;
import id.ac.uhb.week6.fragment.SampleDialogFragment;

public class MainActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    ActivityMainBinding binding;
    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        fragmentManager = getSupportFragmentManager();
        binding.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });
        binding.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });
        binding.button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker();
            }
        });
        binding.button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomeDialog();
            }
        });
        binding.button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogFragment();
            }
        });
        binding.button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialog();
            }
        });
    }

    private void showBottomSheetDialog() {
        SampleBottomDialog sampleBottomDialog = SampleBottomDialog.newInstance("param1","param2");
        sampleBottomDialog.show(fragmentManager,"show_bottom");
    }

    private void showDialogFragment() {
        SampleDialogFragment sampleDialogFragment = SampleDialogFragment.newInstance("param1","param2");
        sampleDialogFragment.show(fragmentManager,"show_dialog");
    }
    OnLoginListener mOnLoginListener = new OnLoginListener() {
        @Override
        public void onLogin(Dialog dialog, boolean done) {
            if(done){
                binding.textSelection.setText("Anda Login");
            } else {
                binding.textSelection.setText("Anda Batal Login");
            }
            dialog.dismiss();
        }
    };
    private void showCustomeDialog() {
        binding.textSelection.setText("Anda memilih Custom Dialog");
        Dialog dialog = new Dialog(this);
        CustomDialogBinding customDialogBinding = CustomDialogBinding.inflate(getLayoutInflater());
        dialog.setTitle("Custom Dialog");
        dialog.setContentView(customDialogBinding.getRoot());
        customDialogBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnLoginListener.onLogin(dialog,false);
            }
        });
        customDialogBinding.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnLoginListener.onLogin(dialog,true);
            }
        });
        dialog.show();
    }

    private void showTimePicker() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog =new TimePickerDialog(this,this,hour,minute,true);
        timePickerDialog.setTitle("Pilih Waktu");
        timePickerDialog.show();
    }


    private void showDatePicker() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            DatePickerDialog datePickerDialog = new DatePickerDialog(this);
            datePickerDialog.setTitle("Pilih Tanggal");
            datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    binding.textSelection.setText(String.format("Pilihan Anda : %d-%d-%d",dayOfMonth,month,year));
                }
            });
            datePickerDialog.show();
        } else {
            Dialog dateDialog = new Dialog(this);
            dateDialog.setTitle("Pilih Tanggal");
            DatePicker datePicker  = new DatePicker(this);
            datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    binding.textSelection.setText(String.format("Pilihan Anda : %d-%d-%d",dayOfMonth,monthOfYear,year));
                }
            });
            dateDialog.setContentView(datePicker);
            dateDialog.show();
        }

    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Judul Alert");
        builder.setMessage("Pesan Alert");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                binding.textSelection.setText("Anda memilih alert dialog standar");
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        binding.textSelection.setText(String.format("Waktu Pilihan Anda - %d:%d",hourOfDay,minute));
    }
}