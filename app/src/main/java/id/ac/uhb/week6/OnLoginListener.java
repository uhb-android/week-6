package id.ac.uhb.week6;

import android.app.Dialog;

public interface OnLoginListener {
    void onLogin(Dialog dialog, boolean done);
}
